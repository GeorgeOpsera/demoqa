(function($) {
    $.fn.sldsCombobox = function(options, callback) {
    	var $this = this;

    	var comboSettings = $.extend(true, {
			combobox: null,
        	comboboxItemsSelector: null,
			listboxItems: null,
        	listboxItemsSelector: null,
			sort: false,
        	options: []
        }, options);

		var $comboboxItems = comboSettings.combobox || $(comboSettings.comboboxSelector);
	    var $listboxItems = (comboSettings.listboxItems || $(comboSettings.listboxItemsSelector)).find('ul.slds-listbox');

	    var isInitialized = false;

	    var renderSvgIconsCombobox = function() {
	    	if(window.renderSvgIcons != null) {
				window.renderSvgIcons();
			}
		};
		
		var clearSelectionCombobox = function(isTriggerOnChange) {

			if(isTriggerOnChange !== false) {
				triggerOnChangeCombobox($this.val());
			}
	    };
		
		var sortDataCombobox = function (data) {
			return data.sort(function (a, b) {
				return a.label.localeCompare(b.label);
            });
		};
		
		var selectItemCombobox = function(id, isTriggerOnChange) {
			
			$this.val(id);

			if(isInitialized) {
				renderSvgIconsCombobox();
			}

			if(isTriggerOnChange) {
				triggerOnChangeCombobox(id);
			}
	    };
		
		var setSelectionCombobox = function(options, isTriggerOnChange) {
			$.each(options, function(i, item) {
				selectItemCombobox(item.index || item, false);
			});

			if(isTriggerOnChange !== false) {
				triggerOnChangeCombobox($this.val());
			}
	    };
				
		var triggerOnChangeCombobox = function(id) {
			$this
				.trigger('change')
				.trigger('changeSelection', [id]);
		};

	    var addItemCombobox = function(id, label) {
			var optionTemplete = '\
				<li role="presentation" class="slds-listbox__item" data-item-id="' + id + '" data-selected="false">\
					<div id="id_' + id + '" class="slds-media slds-listbox__option slds-listbox__option_plain slds-media_small" role="option">\
						<span class="slds-media__figure slds-listbox__option-icon"></span>\
							<span class="slds-media__body">\
								<span class="slds-truncate" title="' + label + '">\
								<span>' + label + '</span>\
							</span>\
						</span>\
					</div>\
				</li>\
			';

			$listboxItems.append(optionTemplete);
	    };
		
		var addGroupedOption = function(index, label) {
			var groupedOptionTemplate = '\
				<div id="' + index + '" class="slds-media slds-listbox__option slds-listbox__option_plain slds-media_small" role="presentation">\
					<h3 class="slds-listbox__option-header" role="presentation">' + label + '</h3>\
				</div>\
			';
			$listboxItems.append(groupedOptionTemplate);
		}
	    	    
	    var setOptionsCombobox = function(options) {
			if (comboSettings.sort) {
				options = sortDataCombobox(options);
				comboSettings.options = options;
			}

	    	$listboxItems.empty();

	    	$.each(options, function(i, item) {
				var escapedItemLabel = $('<span>').text(item.label).html();

				if(item.index.includes('GROUPED_OPTION')){
					addGroupedOption(item.index, escapedItemLabel);
				} else {
					addItemCombobox(item.index, escapedItemLabel);					
				}
		    });
	    };

		$comboboxItems.on('input', function(e) {
			$listboxItems.find('[data-item-id][data-selected="false"]').addClass('slds-hide').filter(function(i, item) {
				return item.innerText.split(/\s+/g).join(' ').toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1;
			}).removeClass('slds-hide');
		});

		$listboxItems.on('click', '[data-item-id]', function(e) {
			var $item = $(this);
			var id = $item.attr('data-item-id');
			
			selectItemCombobox(id, true);

			$item.parents('.slds-dropdown-trigger_click').removeClass('slds-is-open');
			
			//$comboboxItems.val($item[0].innerText.split(/\s+/g).join(''));
			$comboboxItems.val($item[0].innerText.split(/\s+/g).join(' ').trim());
			
			e.stopPropagation();
		});

	    $this.val(null); // Prevent browser cache

	    setOptionsCombobox(comboSettings.options);
		
		if(comboSettings.selection != null) {
			setSelectionCombobox(comboSettings.selection);
	    }
		
		renderSvgIconsCombobox();
		
	    callback(setSelectionCombobox, clearSelectionCombobox, setOptionsCombobox);
	    
	    isInitialized = true;
	    
	    $comboboxItems.on('focus input', function(e) {
	    	$comboboxItems.parents('.slds-dropdown-trigger').toggleClass('slds-is-open', true);
	    	$listboxItems.find('[data-item-id][data-selected="false"]').addClass('slds-hide').filter(function(i, item) {  
				return item.innerText.split(/\s+/g).join(' ').toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1;
			}).removeClass('slds-hide');
	    });
	    
	    return $this
        	.on('clearSelection', function(e) {
	        	clearSelectionCombobox();
	        }).on('setSelection', function(e, selection) {
	        	setSelectionCombobox(selection);
	        });
    };
}(jQuery));
