var getSelectedItems = function(settings, items) {
	var fields = ["uniqueKey", "parentUniqueKey", "relatedFieldName", "name", "label"];
	var _items = [];

	$.each(items, function(i, item) {
		if (item.isSelected) {
			var _item = {};

			$.each(fields, function(i, field) {
				_item[field] = item[field];
			});

			_item[settings.items.childs] = getSelectedItems(
				settings,
				item[settings.items.childs]
			);

			_items.push(_item);
		}
	});

	return _items;
};

var callAsync = function(fn, delay, showGlobalSpinnerFn) {
  var showGlobalSpinner = showGlobalSpinnerFn || window.showGlobalSpinner || parent.showGlobalSpinner || function() {};

	showGlobalSpinner(true);
	setTimeout(function() {
		fn();

		showGlobalSpinner(false);
	}, 100 || delay);
};

var callNested = function(settings, items, fn, parent) {
	$.each(items, function(i, item) {
		// callAsync(function() {
		fn(item, parent);

		if (item[settings.items.childs]) {
			callNested(settings, item[settings.items.childs], fn, item);
		}
		// }, 100);
	});
};

var normalizeUniqueKey = function (itemB, itemA) {
	if (itemB.uniqueKey !== itemA.uniqueKey && itemB.uniqueKey.indexOf('-') === -1 && itemA.uniqueKey.indexOf('-') !== -1) {
		return itemB.uniqueKey + '-' + itemB.parentUniqueKey + '_' + itemB.uniqueKey;
	}
	return itemB.uniqueKey;
}

var mergeObjectsSelection = function(a, b) {
	if (a.uniqueKey !== normalizeUniqueKey(b, a)) return;

	a.isSelected = null;

	$.each(b, function(bkey, bval) {
		// callAsync(function() {
		if (bkey == "uniqueKey") {
			a.isSelected = true;
		}

		if (bkey == "childObjRelStructure") {
			$.each(bval, function(bi, bitem) {
				// callAsync(function() {
				if (a[bkey]) {
					$.each(a[bkey], function(ai, aitem) {
						// callAsync(function() {
						if (aitem.uniqueKey === normalizeUniqueKey(bitem, aitem)) {
							aitem = mergeObjectsSelection(aitem, bitem);
						}
						// });
					});
				}
				// });
			});
		}
		// });
	});

	return a;
};

var mergeParentObjectsSelection = function(a, b) {
	$.each(a, function(ai, aitem) {
		// callAsync(function() {
		$.each(b, function(bi, bitem) {
			// callAsync(function() {
			if (aitem["uniqueKey"] == bitem["uniqueKey"]) {
				aitem.isSelected = true;
				_hasScheduledProcessMultipleSelected = true;
			}
			// });
		});
		// });
	});
};

var setCascadeDeleteParents = function(settings, items) {
	var hasCascadeDelete = function(item) {
		var isCascadeDelete = false;

		if (!isCascadeDelete) {
			$.each(item[settings.items.childs], function(i, item) {
				isCascadeDelete = item.isCascadeDelete;

				if (isCascadeDelete) return;
			});
		}

		if (!isCascadeDelete) {
			$.each(item[settings.items.childs], function(i, item) {
				isCascadeDelete = hasCascadeDelete(item);

				if (isCascadeDelete) return;
			});
		}

		return isCascadeDelete;
	};

	callNested(settings, items, function(item) {
		if (
			item[settings.items.childs] &&
			item[settings.items.childs].length > 0 &&
			!item.isCascadeDelete
		) {
			item.isCascadeDelete = hasCascadeDelete(item);
		}
	});
};

var hasItem = function(items, fn) {
	for (var i = 0; i < items.length; i++) {
		if (fn(items[i], i)) return true;
	}

	return false;
};

var removeDuplicatedUniqueKeys = function(items, uniqueKeys) {
	uniqueKeys = uniqueKeys || [];

	return items
		.reduce(function(ret, item) {
			if (uniqueKeys.indexOf(item.uniqueKey) !== -1) {
				return ret;
			}

			uniqueKeys.push(item.uniqueKey);
			ret.push(item);

			return ret;
		}, [])
		.reduce(function(ret, item) {
			if (
				item["childObjRelStructure"] &&
				item["childObjRelStructure"].length > 0
			) {
				item["childObjRelStructure"] = removeDuplicatedUniqueKeys(
					item["childObjRelStructure"],
					uniqueKeys
				);
			}

			if (uniqueKeys.indexOf(item.uniqueKey) === -1) {
				uniqueKeys.push(item.uniqueKey);
			}

			ret.push(item);

			return ret;
		}, []);
};