window.GRAXFieldDisplay = {
  fillSelect: function(id, items, isMultiple, selected) {
    var select = document.getElementById(id);
    var options = document.createDocumentFragment();
    
    var selectedIndex = -1;
    
    for (i = 0; i < items.length; i++) { 
      var option = document.createElement('option');
      option.value = items[i].val;
      option.textContent = items[i].label;
      option.selected = isMultiple ? items[i].isSelected : items[i].val === selected;
      
      if(option.selected && !isMultiple) {
        selectedIndex = i;
      }
      
      options.appendChild(option);
    }
    
    select.appendChild(options);
    
    if(!isMultiple) {
      select.selectedIndex = selectedIndex;
    }
  },
  fillTemplateList: function(id, items, template) {
    var element = document.getElementById(id);
    var html = '';
    
    for (i = 0; i < items.length; i++) { 
      html += template
        .split('$label').join(items[i].label)
        .split('$val').join(items[i].val);
    }
    
    element.innerHTML = html;
  }
}