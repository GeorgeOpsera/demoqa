(function($) {
	$.fn.sldsPillsComboboxExtend = function(options, callback) {

		var sldsPillsComboboxUsers = $usersSelection.sldsPillsCombobox(options,callback);

		$(options.comboboxSelector).on('focus input', function(e) {
			if (e.target.value.length == 0) {
				sldsPillsComboboxUsers.setOptions(options.usersJSON);

				$(options.listboxItemsSelector + ' > ul.slds-listbox').find('[data-item-id][data-selected="false"]').addClass('slds-hide').filter(function(i, item) {
					return item.innerText.split(/\s+/g).join(' ').toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1;
				}).removeClass('slds-hide');

				return false;
			}

			if (e.target.value.length > 0 && e.target.value.length < 3) {
				sldsPillsComboboxUsers.setOptions([]);
				$(options.comboboxSelector).parents('.slds-dropdown-trigger').toggleClass('slds-is-open', false);
				return false;
			}

			$(options.listboxItemsSelector + ' .slds-listbox.slds-listbox_vertical').attr('data-before','Loading...');

			function functionToEval(val, remoteAction, listboxItemsSelector) {
				Visualforce.remoting.Manager.invokeAction(
					remoteAction,
					val,
					function(response) {
						if(!response) {
							response = '';
						}
						response = JSON.parse(response);
						sldsPillsComboboxUsers.setOptions(response);
						$(listboxItemsSelector + ' .slds-listbox.slds-listbox_vertical').attr('data-before','No results...');
						$(listboxItemsSelector + ' > ul.slds-listbox').find('[data-item-id][data-selected="false"]').addClass('slds-hide').filter(
							function(i, item) {
								return item.innerText.split(/\s+/g).join(' ').toLowerCase().indexOf(val) !== -1;
						}).removeClass('slds-hide');
	
					}, {buffer: true, escape: false, timeout: 120000}
				)
			}

			functionToEval(e.target.value.toLowerCase(), options.searchAPEXFunction, options.listboxItemsSelector);

		});

		return sldsPillsComboboxUsers;

	};
}(jQuery));
