window.initGRAXSecuredIframe = function(domain, hiddenIframeId) {
  var isAuthenticated = false;
  var cookieAuthenticationWindow = null;
  var loadTab = null;

  var iframe = document.getElementById(hiddenIframeId);

  var cookieAuthenticationUrl =
    domain + "/grax-app/public/cookie_authentication.html";

  function processMessage(message) {
    var isCookieVerification = message.data.type === "cookie_verification";
    var isCookieAuthentication = message.data.type === "cookie_authentication";

    if(isCookieVerification || isCookieAuthentication) {
      isAuthenticated = Boolean(message.data.isAuthenticated);
    }

    if(isCookieVerification) {
      document.dispatchEvent(new CustomEvent("GRAXSecuredIframe:init"));
    }

    if(isCookieAuthentication) {
      document.dispatchEvent(new CustomEvent("GRAXSecuredIframe:auth"));
    }

    if (message.data.type === "cookie_authentication") {
      if (cookieAuthenticationWindow != null) {
        cookieAuthenticationWindow.close();
        cookieAuthenticationWindow = null;
      }

      if (loadTab != null && isAuthenticated) {
        loadTab();
      } else {
        loadTab = null;
      }
    }
  };

  window.addEventListener("message", processMessage, false);

  iframe.src =
    cookieAuthenticationUrl +
    "?parent=" +
    encodeURIComponent(window.location.origin);

  try {
    iframe.contentWindow.addEventListener("message", processMessage, false);
  } catch (e) {
    console.log(e);
  }

  function openPopup(e) {
    cookieAuthenticationWindow = window.open(
      cookieAuthenticationUrl +
        "?opener=" +
        encodeURIComponent(window.location.origin),
      "_blank",
      "width=640,height=480"
    );

    try {
      cookieAuthenticationWindow.focus();
    } catch (e) {
      alert(
        "Pop-up Blocker is enabled! Please add this site to your exception list."
      );
    }

    e.preventDefault();
  };

  function graxSecuredIframe(fn, context) {
    return function(e) {
      loadTab = fn.bind(null, context);

      if (isAuthenticated === false) {
        if (e.isTrusted) {
          openPopup(e);
        } else {
          var onRequestClick = new CustomEvent("GRAXSecuredIframe:requestClick", {
            detail: {
              continueAction: openPopup
            }
          });

          document.dispatchEvent(onRequestClick);
        }
      } else if (isAuthenticated) {
        loadTab();
        loadTab = null;
      }

      e.preventDefault();
    };
  };

  window.graxSecuredIframe = graxSecuredIframe;
};
