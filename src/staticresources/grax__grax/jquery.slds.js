$(function() {
    var _ariaAttrToggle = function(i, attr) {
        return !(attr == 'true');
    };

    var _preventDefault = function(e) {
        e.preventDefault();
    };

    $(document)
        
        // Tabs
        .on('click', '[role="tablist"] [role="presentation"] [role="tab"]', function(e) {
            var $currentTab = $(this);
            var $previousTab = $currentTab.closest('[role="tablist"]').find('[aria-selected="true"]');

            $('[role="tabpanel"]#' + $previousTab.attr('aria-controls'))
                .removeClass('slds-show')
                .addClass('slds-hide');
            $('[role="tabpanel"]#' + $currentTab.attr('aria-controls'))
                .removeClass('slds-hide')
                .addClass('slds-show');

            $previousTab.attr('aria-selected', 'false')
                .closest('[role="presentation"]')
                    .removeClass('slds-is-active');
            $currentTab.attr('aria-selected', 'true')
                .closest('[role="presentation"]')
                    .addClass('slds-is-active');
        })

         // Accordions, Sections
        .on('click', '[aria-expanded]', function(e) {
            var $currentAccordion = $(this);
            if(!$currentAccordion.is('[role="treeitem"]')) {
                $currentAccordion.attr('aria-expanded', _ariaAttrToggle);
                $('#' + $currentAccordion.attr('aria-controls'))
                    .attr('aria-hidden', _ariaAttrToggle)
                    .closest('.slds-accordion__section, .slds-section').toggleClass('slds-is-open');

                e.preventDefault();
            }
        })
        
        // Tooltips help
        .on('reRendered', function(e) {
            $('[role="tooltip"][id]').each(function() {
                var $tooltip = $(this);
                var _isBottom = $tooltip.is('.slds-nubbin_bottom-left, .slds-nubbin_bottom-right');
                var _isRight = $tooltip.is('.slds-nubbin_top-right, .slds-nubbin_bottom-right');

                $('[aria-describedby="' + $tooltip.attr('id') + '"]')
                    .removeAttr('title')
                    .on('mouseover mouseleave', function(e) {
                        if(e.type == 'mouseover') {
                            var $this = $(this);

                            var coords = e.target.getBoundingClientRect();

                            $tooltip.css({
                                'top': Math.round(
                                    (_isBottom ? coord.bottom : coords.top) +
                                    ($this.outerHeight(true)
                                        * (_isBottom ? -1 : 1)
                                    )
                                ),
                                'left': Math.round(
                                    coords.left + (_isRight ? $this.outerWidth(true) : 0) - (_isRight ? $tooltip.outerWidth() : 0))
                            });
                        }

                        $tooltip.attr('aria-hidden', _ariaAttrToggle).toggleClass('slds-hide', e.type == 'mouseleave');
                    }).on('click', _preventDefault);
            });
        }).on('mousedown mouseup click', '.slds-popover__body', _preventDefault)

        // Modals
        .on('modaltoggle', '[aria-modal="true"]', function(e, state) {
            $(this)
                .toggleClass('slds-fade-in-open', state)
                .toggleClass('slds-hide', state !== undefined ? !state : null)
                .next('.slds-backdrop')
                    .toggleClass('slds-backdrop_open', state)
                    .toggleClass('slds-hide', state !== undefined ? !state : null);
        })

        .on('click', '[data-toggle="modal"]', function(e) {
            $($(this).data('target')).trigger('modaltoggle');

            _preventDefault(e);
        })

        // Menus
        /*.on('click', function(e) {
            var $trigger = $('.slds-dropdown-trigger_click [aria-haspopup="true"]');

            $trigger.not(e.target).closest('.slds-dropdown-trigger').removeClass('slds-is-open');

            if($trigger.is(e.target)) {
                $(e.target).closest('.slds-dropdown-trigger').addClass('slds-is-open');
                
                _preventDefault(e);
                e.stopPropagation();
            }
        })*/
        .on('click', function(e) { // Menus, Comboboxes
            var $target = $(e.target);
            var $triggers = $('.slds-dropdown-trigger_click');
            
            $triggers.not($target).removeClass('slds-is-open');
            
            if($triggers.has($target).length > 0) {
                $target.closest($triggers).toggleClass('slds-is-open', true);

                _preventDefault(e);
                e.stopPropagation();
            }
        })
        .on('mousedown mouseup click', '.slds-dropdown', function(e) {
            if(!$(this).is('.slds-datepicker')) _preventDefault(e);
        })

        ;

    // FUNCTIONS
    window.goToTab = function(name) {
        $('#tab-' + name + '__item').focus().trigger('click');
    };


    // SVG
    var svgns = 'http://www.w3.org/2000/svg';
    var xlinkns = 'http://www.w3.org/1999/xlink';

    window.renderRings = function() {
        var percentDecimal = function(value) {
            return value / 100;
        };
    
        var getD = function(isLong, arcX, arcY) {
            return 'M 1 0 A 1 1 0 ' + isLong + ' 1 ' + arcX + ' ' + arcY + ' L 0 0';
        };
    
        var calculateD = function(fillPercent) {
            var isLong = fillPercent > 0.5 ? 1 : 0;
            var arcX = Math.cos(2 * Math.PI * fillPercent);
            var arcY = Math.sin(2 * Math.PI * fillPercent);
    
            return getD(isLong, arcX, arcY);
        };
    
        $('.slds-progress-ring').each(function() {
            var $ring = $(this);
            var $progress = $ring.find('.slds-progress-ring__progress');
            var $content = $ring.find('.slds-progress-ring__content');

            var _iconCompleted = $ring.attr('data-icon-completed');
    
            var value = parseFloat($progress.attr('aria-valuenow') || '0');

            var isCompleted = value == 100 && $ring.attr('data-completed') == 'true';

            $ring.toggleClass('slds-progress-ring_complete', isCompleted);

            $content.empty();
            if(isCompleted) {
                $content.append(
                    $('<i>').attr({
                        class: 'slds-icon slds-icon-text-default',
                        'data-icon': _iconCompleted || '/_slds/icons/action-sprite/svg/symbols.svg#approval'
                    })
                )
            }

            var fillPercentDecimal = percentDecimal(value);
    
            var _svg = document.createElementNS(svgns, 'svg');
            _svg.setAttribute('viewBox', '-1 -1 2 2');
    
            var _path = document.createElementNS(svgns, 'path');
            _path.setAttribute('class', 'slds-progress-ring__path');
            _path.setAttribute('d', calculateD(fillPercentDecimal));
    
            _svg.appendChild(_path);
    
            $progress.html(_svg);
        });
    };
    
    window.renderSvgIcons = function() {
        renderRings();

        $('[data-icon]').each(function() {
            var $this = $(this);

            var _svg = document.createElementNS(svgns, 'svg');
            _svg.setAttribute('class', $this.attr('class'));
            _svg.setAttribute('aria-hidden', 'true');

            var _use = document.createElementNS(svgns, 'use');
            _use.setAttributeNS(xlinkns, 'xlink:href', $this.attr('data-icon'));

            _svg.appendChild(_use);

            $this.replaceWith(_svg);
        });
    };
});