(function($) {
    $.fn.sldsPillsCombobox = function(options, callback) {
    	var $this = this;

    	var settings = $.extend(true, {
        	comboboxItemsSelector: null,
        	listboxItemsSelector: null,
        	selectedItemsPillsSelector: null,
        	pillRemoveIcon: '/assets/icons/utility-sprite/svg/symbols.svg#close',
			sort: false,
        	options: []
        }, options);

		var $comboboxItems = $(settings.comboboxSelector);
	    var $listboxItems = $(settings.listboxItemsSelector + ' > ul.slds-listbox');
	    var $selectedItemsPills = $(settings.selectedItemsPillsSelector);

	    var isInitialized = false;

	    var renderSvgIcons = function() {
	    	if(window.renderSvgIcons != null) {
				window.renderSvgIcons();
			}
		};

		var sortData = function (data) {
			return data.sort(function (a, b) {
				return a.label.localeCompare(b.label);
            });
		};


		var triggerOnChange = function(idsSelected) {
			$this
				.trigger('change')
				.trigger('changeSelection', [idsSelected]);
		};

		var getSelectedIds = function() {
	    	return JSON.parse($this.val() || '[]');
	    };

	    var setSelectedIds = function(idsSelected) {
	    	$this.val(JSON.stringify(idsSelected));
	    };

	    var addItem = function(id, label) {
			var optionTemplete = '\
				<li role="presentation" class="slds-listbox__item" data-item-id="' + id + '" data-selected="false">\
					<div id="id_' + id + '" class="slds-media slds-listbox__option slds-listbox__option_plain slds-media_small" role="option">\
						<span class="slds-media__figure slds-listbox__option-icon"></span>\
							<span class="slds-media__body">\
								<span class="slds-truncate" title="' + label + '">\
								<span>' + label + '</span>\
							</span>\
						</span>\
					</div>\
				</li>\
			';

			$listboxItems.append(optionTemplete);
	    };

		var addFieldGroupedOption = function(index, label) {
			var fieldGroupedOptionTemplate = '\
				<div id="' + index + '" class="slds-media slds-listbox__option slds-listbox__option_plain slds-media_small" role="presentation">\
					<h3 class="slds-listbox__option-header" role="presentation">' + label + '</h3>\
				</div>\
			';
			$listboxItems.append(fieldGroupedOptionTemplate);
		}

	    var setOptions = function(options) {
			if (settings.sort) {
				options = sortData(options);
				settings.options = options;
			}

	    	$listboxItems.empty();

	    	$.each(options, function(i, item) {
				var itemEscapedLabel = $('<span>').text(item.label).html();

				if(item.id.includes('GROUPED_OPTIONS')) {
					addFieldGroupedOption(item.id, itemEscapedLabel)
				} else {
					addItem(item.id, itemEscapedLabel);
				}
		    });
	    };

	    var selectItem = function(id, isTriggerOnChange) {
			var idsSelected = getSelectedIds();

			if(!idsSelected.includes(id)){
				var $item = $listboxItems.find('[data-item-id="' + id + '"]');
				var label = $item.text().split(/\s+/g).join(' ');
				var escapedLabel = $('<span>').text(label).html();

				$listboxItems.find('[data-item-id="' + id + '"]').addClass('slds-hide').attr('data-selected', 'true');

				var pillTemplate = '\
					<li class="slds-listbox-item" role="presentation" data-item-id="' + id + '">\
						<span class="slds-pill" role="option" aria-selected="true">\
							<span class="slds-pill__label" title="' + escapedLabel + '">' + escapedLabel + '</span>\
								<span class="slds-icon_container slds-pill__remove" title="Remove">\
								<svg class="slds-button__icon" aria-hidden="true">\
									<use xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="'+settings.pillRemoveIcon+'"></use>\
								</svg>\
							</span>\
						</span>\
					</li>\
				';

				$selectedItemsPills.append(pillTemplate);

				idsSelected.push(id);
				setSelectedIds(idsSelected);

				if(isInitialized) {
					renderSvgIcons();
				}

				if(isTriggerOnChange) {
					triggerOnChange(idsSelected);
				}
			}
	    };

	    var unselectItem = function(id, isTriggerOnChange) {
			$selectedItemsPills.find('[data-item-id="' + id + '"]').remove();

			$listboxItems.find('[data-item-id="' + id + '"]').removeClass('slds-hide').attr('data-selected', 'false');

			var idsSelected = getSelectedIds();
			idsSelected.splice(idsSelected.indexOf(id), 1);
			setSelectedIds(idsSelected);

			if(isTriggerOnChange) {
				triggerOnChange(idsSelected);
			}
	    };


	    var clearSelection = function(isTriggerOnChange) {
			/*$.each(settings.options, function(i, item) {
				unselectItem(item.id, false);
			});*/

			setSelectedIds([]);
			$selectedItemsPills.empty();

			if(isTriggerOnChange !== false) {
				triggerOnChange(getSelectedIds());
			}
	    };

	    var setSelection = function(options, isTriggerOnChange) {
			$.each(options, function(i, item) {
				selectItem(item.id || item, false);
			});

			if(isTriggerOnChange !== false) {
				triggerOnChange(getSelectedIds());
			}
	    };

		$comboboxItems.on('input', function(e) {
			$listboxItems.find('[data-item-id][data-selected="false"]').addClass('slds-hide').filter(function(i, item) {
				return item.innerText.split(/\s+/g).join(' ').toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1;
			}).removeClass('slds-hide');
		});

		$listboxItems.on('click', '[data-item-id]', function(e) {
			var $item = $(this);
			var id = $item.attr('data-item-id');

			selectItem(id, true);

			var list = $(settings.listboxItemsSelector + ' ul li');
			$comboboxItems.val('');
			$comboboxItems.trigger('input');

			$item.parents('.slds-dropdown-trigger_click').removeClass('slds-is-open');
            $($comboboxItems).parents('.slds-dropdown-trigger').toggleClass('slds-is-open', false);

			list.each(function(index) {
				index = $(this).attr("data-item-id");
				if (index.toString() != id && $item.hasClass("slds-hide") && $selectedItemsPills.find('[data-item-id="' + id + '"]').length == 0) {
					$listboxItems.find('[data-item-id="' + index + '"]').removeClass('slds-hide').attr('data-selected', 'false');
				};
			});
			e.stopPropagation();
		});

	    $selectedItemsPills.on('click', '.slds-listbox-item', function(e) {
			var id = $(this).attr('data-item-id');

			if($(e.target).is('.slds-pill__remove, .slds-pill__remove *')) {
				unselectItem(id, true);
			}
	    });

	    $this.val(null); // Prevent browser cache

	    setOptions(settings.options);

	    if(settings.selection != null) {
			setSelection(settings.selection);
	    }

	    renderSvgIcons();

	    callback(setSelection, clearSelection, setOptions);

	    isInitialized = true;

	    $comboboxItems.on('focus input', function(e) {
	    	$comboboxItems.parents('.slds-dropdown-trigger').toggleClass('slds-is-open', true);
	    	$listboxItems.find('[data-item-id][data-selected="false"]').addClass('slds-hide').filter(function(i, item) {
				return item.innerText.split(/\s+/g).join(' ').toLowerCase().indexOf(e.target.value.toLowerCase()) !== -1;
			}).removeClass('slds-hide');
	    });

	    $this.setOptions = setOptions;

        return $this
        	.on('clearSelection', function(e) {
	        	clearSelection();
	        }).on('setSelection', function(e, selection) {
	        	setSelection(selection);
	        });
    };
}(jQuery));
