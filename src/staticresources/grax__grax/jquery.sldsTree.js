(function($) {
	var svgns = 'http://www.w3.org/2000/svg';
	var xlinkns = 'http://www.w3.org/1999/xlink';

    $.fn.sldsTree = function(options) {
    	var $this = this;

    	var settings = $.extend(true, {
        	data: [],
        	items: {
        		id: 'id',
        		label: 'label',
        		childs: 'childs',
        	},
        	attrs: {
				root: {'class': 'slds-tree_container'},
				ul: {'class': 'slds-tree'},
				li: {
					'role': 'treeitem',
					'aria-level': '1',
					'aria-expanded': 'false'
				},
				div: {'class': 'slds-tree__item'},
				button: {
					'class': 'slds-button slds-button_icon slds-m-right_x-small',
					'aria-hidden': 'true',
					'tabindex': '-1'
				}
			},
			expandButton: {
				'disabled': 'slds-is-disabled'
			},
			expandIcon: {
				'class': 'slds-button__icon slds-button__icon_small',
				'path': '/assets/icons/utility-sprite/svg/symbols.svg#chevronright'
			},
			onStart: function($root, items, settings, callback) {
				callback($root);
			},
        	onUl: function($ul, items, settings, callback) {
				callback($ul);
			},
        	onLi: function($li, item, settings, callback) {
				callback($li);
			},
        	onDiv: function($div, item, settings, callback) {
				$div.append(item[settings.items.label]);
				
				callback($div);
        	},
        	onButton: function($button, item, settings, callback) {
				callback($button);
			},
            onFinish: function($root, items, settings, callback) {
				callback($root);
			}
        }, options);

        var getTreeItemButton = function(item, disabled, $li, callback) {
			setTimeout(function() {
				var $button = $('<button>').attr(settings.attrs.button).toggleClass(settings.expandButton.disabled, disabled);

				var icon = document.createElementNS(svgns, 'svg');
				icon.setAttribute('class', settings.expandIcon.class);
				icon.setAttribute('aria-hidden', 'true');

				var icon_use = document.createElementNS(svgns, 'use');
				icon_use.setAttributeNS(xlinkns, 'xlink:href', settings.expandIcon.path);
				
				icon.appendChild(icon_use);

				$button.append(icon);

				$button.on('click', function(e) {
					$li.attr('aria-expanded', function(i, attr) {
						return !(attr == 'true');
					});

					e.preventDefault();
				});

				setTimeout(function() {
					settings.onButton($button, item, settings, function($button) {
						callback($button);
					});					
				}, 1);
			}, 1);
        }

        var getTreeItemContent = function(item, level, $ul, $li, callback) {
        	var $div = $('<div>').attr(settings.attrs.div);

			getTreeItemButton(item, !item[settings.items.childs] || item[settings.items.childs].length < 1, $li, function($button) {
				$div.append($button);

				setTimeout(function() {
					settings.onDiv($div, item, settings, function() {
						callback($div);
					});
				}, 1);
			});
        }

        var getTreeItem = function(item, level, $ul, callback) {
        	var $li = $('<li>').attr(settings.attrs.li);

			getTreeItemContent(item, level, $ul, $li, function($div) {
				$li.attr({'aria-level': level}).append($div);

				if(item[settings.items.childs]) {
					getTree(item[settings.items.childs], level+1, function($ul) {
						$li.append($ul);

						setTimeout(function() {
							settings.onLi($li, item, settings, function($li) {
								callback($li);
							});			
						}, 1);
					});
				} else {
					callback($li);
				}
			});
        };

        var getTree = function(items, level, callback) {
        	var $ul = $('<ul>').attr(settings.attrs.ul);

        	if(level > 1) $ul.attr({'role': 'group'});
			
			var addTreeItems = function(items, i, callback) {
				if(!items || i == items.length) {
					callback($ul);
				} else {
					getTreeItem(items[i], level, $ul, function($li) {
						$ul.append($li);

						setTimeout(function() {
							addTreeItems(items, i + 1, callback);
						}, 1);
					});
				}
			};

			addTreeItems(items, 0, function($ul) {
				setTimeout(function() {
					settings.onUl($ul, items, settings, function($ul) {
						callback($ul);
					});					
				}, 1);
			});
		};

		settings.onStart($this, settings.data, settings, function($root) {
			getTree(settings.data, 1, function($_ul) {
				$this.attr(settings.attrs.root).html($_ul);
	
				settings.onFinish($this, settings.data, settings, function($root) {});
			});
		});

        return $this;
    };
 
}(jQuery));